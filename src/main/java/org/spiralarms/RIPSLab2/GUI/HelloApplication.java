package org.spiralarms.RIPSLab2.GUI;

import javafx.application.Application;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.stage.Stage;
import javafx.util.Pair;
import org.spiralarms.RIPSLab2.Model.*;

import java.io.IOException;

public class HelloApplication extends Application {
    private Company company;
    private ListView<String> departmentList;
    private ListView<String> employeeList;
    private ObservableList<String> departmentItems;
    private ObservableList<String> employeeItems;
    TableView<Pair<String, String>> table;
    ObservableList<Pair<String, String>> tableData;

    @Override
    public void start(Stage stage) throws IOException {
        company = initCompany();

        StackPane root = new StackPane();
        GridPane gridPane = new GridPane();

        departmentList = new ListView<String>();
        departmentItems = FXCollections.observableArrayList();
        for (Department dept : company.getDepartments()) {
            departmentItems.add(dept.getName());
        }
        departmentList.setItems(departmentItems);

        employeeList = new ListView<String>();
        employeeItems = FXCollections.observableArrayList();
        employeeList.setItems(employeeItems);

        table = new TableView<>();
        TableColumn<Pair<String, String>, String> colProperty = new TableColumn<>("Property");
        TableColumn<Pair<String, String>, String> colValue = new TableColumn<>("Value");
        tableData = FXCollections.observableArrayList();
        table.setItems(tableData);
        table.getColumns().add(colProperty);
        table.getColumns().add(colValue);
        colProperty.prefWidthProperty().bind(table.widthProperty().multiply(0.7));
        colValue.prefWidthProperty().bind(table.widthProperty().multiply(0.3));
        colProperty.setCellValueFactory(data -> new SimpleStringProperty((data.getValue().getKey())));
        colValue.setCellValueFactory(data -> new SimpleStringProperty((data.getValue().getValue())));

        departmentList.getSelectionModel().selectedIndexProperty().addListener((observableValue, number, t1) -> onDepartmentSelected());
        employeeList.getSelectionModel().selectedIndexProperty().addListener((observableValue, number, t1) -> onEmployeeSelected());

        Label lDepartments = new Label("Departments");
        lDepartments.setStyle("-fx-font-weight: bold");
        Label lEmployees = new Label("Employees");
        lEmployees.setStyle("-fx-font-weight: bold");
        gridPane.add(lDepartments, 0, 0);
        gridPane.add(departmentList, 0, 1);
        gridPane.add(lEmployees, 1, 0);
        gridPane.add(employeeList, 1, 1);
        gridPane.add(table, 2, 1)
        ;
        RowConstraints rowConstraintsGrow = new RowConstraints();
        rowConstraintsGrow.setVgrow(Priority.ALWAYS);
        RowConstraints fixedRow = new RowConstraints(25);
        fixedRow.setVgrow(Priority.NEVER);
        RowConstraints growingRow = new RowConstraints();
        growingRow.setVgrow(Priority.ALWAYS);
        ColumnConstraints column1Constraints = new ColumnConstraints();
        column1Constraints.setPercentWidth(20);
        ColumnConstraints column3Constraints = new ColumnConstraints();
        column3Constraints.setPercentWidth(60);
        gridPane.getRowConstraints().addAll(fixedRow, growingRow);
        gridPane.getColumnConstraints().addAll(column1Constraints, column1Constraints, column3Constraints);
        gridPane.setAlignment(Pos.CENTER);
        root.getChildren().add(gridPane);

        Scene scene = new Scene(root, 640, 480);

        stage.setTitle("Company Departments");
        stage.setScene(scene);
        stage.show();
    }

    public void onDepartmentSelected() {
        int departmentIndex = departmentList.getSelectionModel().getSelectedIndex();
        employeeItems.clear();
        if (departmentIndex != -1) {
            for (Employee e : company.getDepartments().get(departmentIndex).getEmployees()) {
                employeeItems.add(e.getName());
            }
        }
    }
    public void onEmployeeSelected() {
        int employeeIndex = employeeList.getSelectionModel().getSelectedIndex();
        int departmentIndex = departmentList.getSelectionModel().getSelectedIndex();
        tableData.clear();
        if (employeeIndex != -1 && departmentIndex != -1) {
            Employee e = company.getDepartments().get(departmentIndex).getEmployees().get(employeeIndex);
            tableData.add(new Pair<>("Name", e.getName()));
            tableData.add(new Pair<>("Years employed", Integer.toString(e.getYearsEmployed())));
            tableData.add(new Pair<>("Wage", Double.toString(e.getWage())));
        }
    }

    private static Company initCompany() {
        Company company = new Company("My Company");

        Department deptProgrammers = new Department("Programmers");
        company.addDepartment(deptProgrammers);
        Department deptAccounting = new Department("Accounting");
        company.addDepartment(deptAccounting);

        deptProgrammers.addEmployee(EmployeeFactory.getEmployee(EmployeeType.MANAGER,"Petya", 2232, 8));
        deptProgrammers.addEmployee(EmployeeFactory.getEmployee(EmployeeType.EMPLOYEE,"Vasya", 1232, 2));
        deptProgrammers.addEmployee(EmployeeFactory.getEmployee(EmployeeType.EMPLOYEE,"Pasha", 1005, 0));
        deptAccounting.addEmployee(EmployeeFactory.getEmployee(EmployeeType.STAFF,"Daniil", 1322, 4));

        return company;
    }
    public static void main(String[] args) {
        launch();
    }
}