package org.spiralarms.RIPSLab2;

import org.spiralarms.RIPSLab2.Model.*;

public class Main {

    public static void main(String[] args) {
	    Company company = new Company("My Company");

        Department deptProgrammers = new Department("Programmers");
        company.addDepartment(deptProgrammers);
        Department deptAccounting = new Department("Accounting");
        company.addDepartment(deptAccounting);

        deptProgrammers.addEmployee(EmployeeFactory.getEmployee(EmployeeType.MANAGER,"Petya", 2232, 8));
        deptProgrammers.addEmployee(EmployeeFactory.getEmployee(EmployeeType.EMPLOYEE,"Vasya", 1232, 2));
        deptProgrammers.addEmployee(EmployeeFactory.getEmployee(EmployeeType.EMPLOYEE,"Pasha", 1005, 0));
        deptAccounting.addEmployee(EmployeeFactory.getEmployee(EmployeeType.STAFF,"Daniil", 1322, 4));

        System.out.println("Total employees:" + company.countEmployees());
        int minYearsEmployed = 2;
        System.out.println("Employees employed for at least " + minYearsEmployed + " years:");
        for (Employee e : company.getEmployeesWithExperience(minYearsEmployed)) {
            System.out.println(e);
        }
    }
}