package org.spiralarms.RIPSLab2.Model;

public class Manager extends Employee {

    public Manager(String name, double wage, int yearsEmployed) {
        super(name, wage, yearsEmployed);
    }
}
