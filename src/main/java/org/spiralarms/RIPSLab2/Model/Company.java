package org.spiralarms.RIPSLab2.Model;

import java.util.ArrayList;

public class Company {
    private String name;
    private ArrayList<Department> departments = new ArrayList<>();

    public Company(String name) {
        this.name = name;
    }

    public Company(String name, ArrayList<Department> departments) {
        this.name = name;
        this.departments = departments;
    }

    public void addDepartment(Department department) {
        departments.add(department);
    }

    public void removeDepartment(Department department) {
        departments.remove(department);
    }

    public int countEmployees() {
        int result = 0;
        for (Department department : departments) {
            result += department.countEmployees();
        }
        return result;
    }

    public ArrayList<Employee> getEmployees() {
        ArrayList<Employee> result = new ArrayList<>();
        for (Department department : departments) {
            result.addAll(department.getEmployees());
        }
        result.sort(new EmployeeComparator());
        return result;
    }

    public ArrayList<Employee> getEmployeesWithExperience(int minYearsEmployed) {
        ArrayList<Employee> result = new ArrayList<>();
        for (Department department : departments) {
            result.addAll(department.getEmployeesWithExperience(minYearsEmployed));
        }
        result.sort(new EmployeeComparator());
        return result;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Department> getDepartments() {
        return departments;
    }
}
