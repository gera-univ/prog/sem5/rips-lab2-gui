module org.spiralarms.ripslab2.gui {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.xml;


    opens org.spiralarms.RIPSLab2.GUI to javafx.fxml;
    exports org.spiralarms.RIPSLab2.GUI;
}